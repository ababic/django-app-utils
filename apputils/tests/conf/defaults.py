INTEGER_SETTING = 1

BOOLEAN_SETTING = False

STRING_SETTING = 'stringy'

TUPLES_SETTING = (
    (1, 'One'),
    (2, 'Two'),
    (3, 'Three'),
    (4, 'Four'),
)

MODEL_SETTING = 'tests.DefaultModel'

CLASS_SETTING = 'tests.classes.DefaultClass'

MODULE_SETTING = 'tests.modules.default_module'
